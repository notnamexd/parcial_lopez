# Docker_parcial

### Docker con 3 Lenguajes: (Php, Python y Golang)

## Se debe clonar el repositorio y luego en la carpeta raiz generar un archivo .env que contenga las variables y los puertos indicados:

    PHP_PORT=8080
    PYTHON_PORT=5000
    ADMIN_USER=admin
    ADMIN_PASSWORD=admin123

# Una ves generado el archivo procederemos a levantar el docker de la siguiente manera:

    1-Presionamos ctrl + alt + t (para abrir una terminal).
    2-Con cd nos movemos hacia la carpeta docker_parcial.
    3-Ejecutamos el comando" sudo docker-compose -f docker-compose.prod.yml up -d " y esperamos a que levante.

# Una ves levantado el docker, abrimos nuestro navegador web y nos dirigimos al siguien enlace:

    http://localhost:8080/loginn.html

# Aqui podremos loguearnos, registrarnos e incluso recuperar la contraseña.(si entraremos como Administrador debemos ingresar las credenciales correspondientes al archivo .env)