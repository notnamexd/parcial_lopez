<?php
$new_user=$_POST["username"];
$code_act=$_POST["codeact"];
$new_password=$_POST["newpass"];

$ok=0;
$archivo=fopen('Bd/data_user.dat','r+') or die ("Error de apertura de archivo, consulte con el administrador...");
$archivo_new=fopen('Bd/data_temp.dat','a+') or die ("Error de apertura de archivo tmp, consulte con el administrador...");

// Establecer todos los permisos para el archivo temporal
if (!chmod('Bd/data_temp.dat', 0777)) {
    die('No se pudieron establecer los permisos necesarios para el archivo temporal.');
}

while(!feof($archivo)) {
    $linea=fgets($archivo);
    if (strlen($linea)>1) {
        $datos=explode("|",$linea);
        $user= $datos[0];
        $first_name= $datos[1];
        $last_name=$datos[2];
        $code_verif=$datos[3];
        $active=trim($datos[4]);

        if(strcmp($new_user,$user)==0 ) {
            if ($active=='0') {
                $ok=1;
                $active=1;
                $code_verif=$new_password;
            } else {
                $ok=2;
            }
        }        
        fputs($archivo_new, $user."|".$first_name."|".$last_name."|".$code_verif."|".$active."\n");
    }
}

// si es igual usuario y no está activo, comienzo el proceso de cambiar el valor
fclose($archivo);
fclose($archivo_new);
unlink('Bd/data_user.dat');
rename('Bd/data_temp.dat','Bd/data_user.dat');

if ($ok==1) {
    echo'<script type="text/javascript">
    alert("El usuario se activó satisfactoriamente.");
    window.location.href="/scripts/frontend/principal.php";
    </script>';
} elseif ($ok==2) {
    echo'<script type="text/javascript">
    alert("El usuario ya estaba activo.");
    window.location.href="new_pass";
    </script>';
} else {
    echo'<script type="text/javascript">
    alert("No se encontró el usuario requerido.");
    window.location.href="/scripts/frontend/activacion.html";
    </script>';
}
?>
