<?php
session_start();

$data_user = 'Bd/data_user.dat';
$admin_user = getenv('ADMIN_USER');
$admin_password = getenv('ADMIN_PASSWORD');
$user = $_POST["username"];
$passuser = $_POST["password"];
$flag_user = 0;

if ($user === $admin_user && $passuser === $admin_password) {
    // Inicia la sesión para el administrador
    $_SESSION['username'] = $admin_user;
    header('Location: /scripts/frontend/admin_principal.php');
    exit;
} else {
    if (!file_exists($data_user)) {
        echo '<script>alert("Archivo de datos de usuarios no existe");</script>';
    } else {
        $archivo = fopen($data_user, 'r') or die("No puedo abrir archivo de datos para lectura");
        while (!feof($archivo) && $flag_user == 0) {
            $linea = fgets($archivo);
            $datos = explode("|", $linea);
            $users = $datos[0];
            $passusers = $datos[3];

            if (strcmp($users, $user) == 0 && strcmp($passuser, $passusers) == 0) {
                // Usuario autenticado correctamente
                $flag_user = 1;
                break;
            }
        }
        fclose($archivo);

        if ($flag_user == 0) {
            echo "<script>
                    alert('Datos incorrectos');
                    window.location= '/scripts/frontend/loginn.html';
                </script>";
        } else {
            // Inicia la sesión para el usuario autenticado
            $_SESSION['username'] = $user;
            header("Location: /scripts/frontend/principal.php");
            exit;
        }
    }
}
?>

