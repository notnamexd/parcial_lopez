<?php
session_start();

// Elimina todas las variables de sesión
session_unset();

// Destruye la sesión
session_destroy();

// Redirige al usuario a la página de inicio de sesión o a donde prefieras
header("Location: /scripts/frontend/loginn.html");
exit();
?>
