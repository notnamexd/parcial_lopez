<?php
require_once './phpMail/enviarMail.php';
$configuracion = 'config.dat';
$data_user = 'Bd/data_user.dat';
$user = $_POST["username"];
$code_verif = cripto_6(8);
$flag_user = 0;
$ok = 0; 

$asuntoMail = 'Recuperacion de contraseña';
$ruta = "http://localhost:8080/scripts/frontend/activacion.html";
$contenido = '
    <html>
        <head>
            <meta charset="UTF-8">
            <title></title>
        </head>
        <body>
            <div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                <h1>Recuperacion de contraseña.</h1>
                <div align="left" >
                    
                    <p>Este es tu Usuario:  <strong>' . $user . '</strong>.</p>
                    <p><strong>Este es tu código de validación y contraseña por ahora: </strong></p>
                    <br>
                    <p><strong>' . $code_verif  .'</strong></p>
                    <br>
                    <p><a href='.$ruta.'>Cambiar contraseña</a></p>
                    <br>
                    <p><strong>¡¡¡ GRACIAS !!! </strong>.</p>
                </div>
            </div>
        </body>
    </html>';

// Verificar si el archivo de datos existe
if (!file_exists($data_user)) {
    echo '<script>alert("El archivo de configuracion no existe");</script>';
} else {
    // Abrir el archivo para buscar el usuario
    $archivo = fopen($data_user, 'r+') or die("No puedo abrir archivo de datos para lectura");

    // Abrir archivo temporal para escritura
    $archivo_new = fopen('Bd/data_temp.dat', 'w+') or die ("Error de apertura de archivo tmp, consulte con el administrador...");

    while (!feof($archivo)) {
        $linea = fgets($archivo);
        if (strlen($linea) > 1) {
            $datos = explode("|", $linea);
            $users = $datos[0];
            $first_name = $datos[1];
            $last_name = $datos[2];
            $old_password = $datos[3];
            $active = trim($datos[4]);

            if (strcmp($users, $user) == 0) {
                $ok = 1;
                $old_password = $code_verif; // Actualizamos la contraseña
                $active = 0; // Desactivamos la cuenta
            }
            
            // Escribir línea en archivo temporal
            fputs($archivo_new, $users."|".$first_name."|".$last_name."|".$old_password."|".$active."\n");
        }
    }

    fclose($archivo);
    fclose($archivo_new);

    // Eliminar archivo original y renombrar archivo temporal
    unlink($data_user);
    rename('Bd/data_temp.dat', $data_user);

    if ($ok == 1) {
        // Enviar correo electrónico solo si se encontró el usuario y se actualizó correctamente
        $archivo=fopen($configuracion,'r') or die("no puedo abrir archivo de datos");
      while(!feof($archivo)) 
      {
        $linea=fgets($archivo);
        $datos=explode("|",$linea);
        $desde=$datos[0];
        $credencial=$datos[1];
      }
        enviarMail($user, $contenido, $asuntoMail, $adjunto='', $desde, $credencial);
        echo '<script type="text/javascript">
                alert("Codigo de recuperacion enviado a su email.");
                window.location.href="/scripts/frontend/activacion.html";
              </script>';
    } else {
        echo '<script type="text/javascript">
                alert("No se encontro el usuario requerido.");
                window.location.href="/scripts/frontend/recuperar.html";
              </script>';
    }
}

// Función de criptografía
function cripto_6($len) {
  return substr(sha1(time()), 0, $len);
}
?>
