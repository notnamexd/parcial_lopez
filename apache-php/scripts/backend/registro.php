<?php
require_once './phpMail/enviarMail.php';

$configuracion = 'config.dat';
$data_user = 'Bd/data_user.dat';
$user = $_POST["visitor_email"];
$first_name = $_POST["visitor_first_name"];
$last_name = $_POST["visitor_last_name"];
$code_verif = substr(sha1(time()), 0, 8); // Generar código de verificación
$flag_user = 0;
$asuntoMail = 'Activación de cuenta';
$ruta = "http://localhost:8080//scripts/frontend/activacion.html";
$contenido = '
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Activación de cuenta</title>
        </head>
        <body>
            <div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
                <h1>Te registraste con éxito.</h1>
                <div align="left" >
                    <p>Te damos la bienvenida  <strong>' . $first_name . '</strong>.</p>
                    <p>Este es tu Usuario:  <strong>' . $user . '</strong>.</p>
                    <p><strong>Este es tu código de validación y contraseña por ahora: </strong></p>
                    <br>
                    <p><strong>' . $code_verif  .'</strong></p>
                    <br>
                    <p><a href='.$ruta.'>Activar cuenta</a></p>
                    <br>
                    <p><strong>¡¡¡ GRACIAS !!! </strong>.</p>
                </div>
            </div>
        </body>
    </html>';

// Intentar crear el directorio Bd si no existe
if (!file_exists($data_user)) {
    // Creamos el directorio Bd si no existe
    if (!is_dir(dirname($data_user)) && !mkdir(dirname($data_user), 0777, true)) {
        die('No se pudo crear el directorio para almacenar los datos de usuario.');
    }
    
    // Otorgamos permisos al directorio Bd
    if (!chmod(dirname($data_user), 0777)) {
        die('No se pudieron otorgar los permisos necesarios al directorio.');
    }
    
    // Creamos el archivo data_user.dat
    if (!touch($data_user)) {
        die('No se pudo crear el archivo de datos de usuario.');
    }
}

// Verificar existencia de archivo de configuración
if (!file_exists($configuracion)) {
    die('El archivo de configuración no existe.');
}

// Leer configuración para obtener credenciales de correo
$archivo = fopen($configuracion, 'r') or die("No se pudo abrir el archivo de configuración");
while (!feof($archivo)) {
    $linea = fgets($archivo);
    $datos = explode("|", $linea);
    $desde = $datos[0];
    $credencial = $datos[1];
}
fclose($archivo);

// Verificar si el usuario ya existe en data_user.dat
$archivo = fopen($data_user, 'r') or die("No se pudo abrir archivo de datos para lectura");
while (!feof($archivo) && $flag_user == 0) {
    $linea = fgets($archivo);
    $datos = explode("|", $linea);
    $users = $datos[0];
    $active = $datos[4];

    if (strcmp($users, $user) == 0) {
        $flag_user = 1;
        break;
    }
}
fclose($archivo);

// Si el usuario no existe, agregarlo a data_user.dat y enviar correo de activación
if ($flag_user == 0) {
    $archivo = fopen($data_user, 'a+') or die("No se pudo abrir archivo de usuarios para escritura");
    fputs($archivo, $user . "|" . $first_name . "|" . $last_name . "|" . $code_verif . "|" . $flag_user . "\n");
    fclose($archivo);

    // Envío de correo electrónico
    if (enviarMail($user, $contenido, $asuntoMail, '', $desde, $credencial)) {
        echo '<script type="text/javascript">
                alert("Correo enviado correctamente. Se ha enviado un código de activación a tu correo.");
                window.location.href = "/scripts/frontend/activacion.html";
              </script>';
    } else {
        echo '<script type="text/javascript">
                alert("Error al enviar el correo electrónico. Por favor, inténtalo más tarde.");
                window.location.href = "/scripts/frontend/registro.html";
              </script>';
    }
} else {
    echo '<script>alert("El usuario ya existe.");</script>';
}

?>
