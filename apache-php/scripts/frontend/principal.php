<?php
    session_start();
    // Verificar si la sesión no está activa
    if (!isset($_SESSION['username'])) {
        // Redirigir al usuario al formulario de inicio de sesión
        header("Location: /scripts/frontend/loginn.html");
        exit;
    }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link href="styles/principal.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
</head>
<body>
 
    <div class="sidebar">
        <div class="header">Portal</div>
        <ul>
            <li><a href="/scripts/frontend/principal.php">Inicio</a></li>
            <li><a href="#">Perfil</a></li>
            <li><a href="#">Generar Archivos</a></li>
            <li><a href="../backend/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>
    <div class="content">
        <h2>Bienvenido a tu Portal</h2>
        <div class="info">
            <h3>Información Importante</h3>
            <p>Aquí puedes escribir cualquier información relevante para los usuarios de tu aplicación.</p>
            <p>Puedes añadir más párrafos según sea necesario.</p>
        </div>
    </div>
    <div class="content">
        <h2>Bienvenido a tu Portal</h2>
        <div class="info">
            <h3>Información Importante</h3>
            <p>Aquí puedes escribir cualquier información relevante para los usuarios de tu aplicación.</p>
            <p>Puedes añadir más párrafos según sea necesario.</p>
        </div>
    </div>
</body>
</html>
