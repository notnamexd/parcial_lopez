package main

import (
    "fmt"
    "net/http"
	"net"
	"os"
	"encoding/csv"
)

func getServerIP() (string, error) {
    addrs, err := net.InterfaceAddrs()
    if err != nil {
        return "", err
    }

    for _, addr := range addrs {
        var ip net.IP
        switch v := addr.(type) {
        case *net.IPNet:
            ip = v.IP
        case *net.IPAddr:
            ip = v.IP
        }
        if ip != nil && !ip.IsLoopback() && ip.To4() != nil {
            return ip.String(), nil
        }
    }

    return "", fmt.Errorf("Ups,parece que ha surgido un problema al buscar IP Address")
}

func validateToken(token string) bool {
    if token == "admin123"{
		return true
	}
	return false
}

func handler(w http.ResponseWriter, r *http.Request) {
    token := r.Header.Get("Authorization")

    if !validateToken(token) {
        http.Error(w, "Token Invalido", http.StatusUnauthorized)
        return
    }
	idprovince:=r.URL.Query().Get("province")
	if idprovince==""{
        http.Error(w, "Id provincia is required", http.StatusBadRequest)
        return
    }

	file, err := os.Open("provinci.dat")
	if err != nil {
		http.Error(w, "No se puede acceder a los datos", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	reader := csv.NewReader(file)
    reader.Comma = '|'

    records, err := reader.ReadAll()
    if err != nil {
        http.Error(w, "Error leyendo archivo", http.StatusInternalServerError)
        return
    }
    var found bool
    for _, record := range records {
        if record[0] == idprovince {
            found = true
            fmt.Fprintf(w, "Provincia: %s\n", record[1])
            break
        }
    }
    if !found {
        http.Error(w, "Id Province not found", http.StatusNotFound)
    }
}

func main() {
	ip, err := getServerIP()
    if err != nil {
        fmt.Println("Error al obtener la IP para levantar el servicio:", err)
        os.Exit(1)
    }
    http.HandleFunc("/provincia", handler)
	var ServidorApi= ip+":8080"
    fmt.Println("Arrancando Servicio en ",ServidorApi)
	fmt.Println(ServidorApi)
    http.ListenAndServe(ServidorApi, nil)
}
